@echo off
title Test script

:start
echo Starting Test script.
echo.

java -Xms512m -Xmx512m -cp ./build/libs/template.jar com.gitlab.luksdlt92.template.Template

if ERRORLEVEL 2 goto restart
if ERRORLEVEL 1 goto error
goto end

:restart
echo.
echo Test script restarted.
echo.
goto start

:error
echo.
echo Test script Terminated Abnormally!
echo.

:end
echo.
echo Test script Terminated.
echo.
pause